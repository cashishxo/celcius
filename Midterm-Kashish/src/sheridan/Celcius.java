package sheridan;

public class Celcius {
	public static int fromFarenheit(int faren) {
		return Math.round((faren-32) * ( 5 / 9));
	}
}
